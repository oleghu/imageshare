## Installering og oppsett av prosjektet

Clone the repository down to your wished location on you computer by using the command:

`git clone https://gitlab.stud.idi.ntnu.no/oleghu/imageshare.git`


Navigate into the folder "ImageShare"

`cd ImageShare`


download the packages from npm

`npm i`


Run expo client with the command:

`expo start`

Choose the emulator/simulator of your wish. The easiest is to use the expo app
